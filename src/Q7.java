import java.util.Scanner;

public class Q7 {
    public static void main(String args[]){
        System.out.print("請輸入3個數值:");
        Scanner scanner = new Scanner(System.in);
        int[] a = new int[3];
        for(int i=0;i<3;i++) {
            a[i] = scanner.nextInt();
        }
        int temp;
        for(int j=0;j<3;j++){
            for(int i=0;i<2;i++) {
                if (a[i] > a[i + 1]) {
                    temp = a[i];
                    a[i] = a[i + 1];
                    a[i + 1] = temp;
                }
            }
        }
        for(int i=0;i<3;i++){
            System.out.print(a[i]+" ");
        }
    }
}
