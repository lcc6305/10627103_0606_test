import java.util.Random;

public class Q2 {
    public static void main(String[] args) {

        Random ran = new Random();
        int ranone = ran.nextInt(+10) + 10;
        int[] rannumber;
        rannumber = new int[ranone];
        System.out.println("共"+ranone+"筆");
        for (int i = 0; i < ranone; i++) {
            rannumber[i] = ran.nextInt(999);
        }
        for (int i = 0; i < ranone; i++) {
            if (rannumber[i] < 100)
                System.out.println("第"+(i+1)+"筆是 "+rannumber[i]+" 不是三位數");
            else {
                int sum = 0, product = 1, difference = rannumber[i] / 100;
                int testsum = rannumber[i];

                for (int j = 0; j < 3; j++) {
                    sum += testsum % 10;
                    testsum /= 10;
                }

                int testproduct = rannumber[i];
                for (int j = 0; j < 3; j++) {
                    product *= testproduct % 10;
                    testproduct /= 10;
                }

                int testdifferece = rannumber[i] % 100;
                for (int j = 0; j < 2; j++) {
                    difference -= testdifferece % 10;
                    testdifferece /= 10;
                }
                System.out.println("第"+(i+1)+"筆輸入的數是:" + rannumber[i] + " 每位數和:" + sum + " 積和:" + product + " 差:" + difference);
            }
        }
    }
}
