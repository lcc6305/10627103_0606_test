import java.util.Scanner;

public class Q5 {
    public static void main(String args[]) {
        System.out.print("請輸入N:");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int min;
        int s = 0;
        int[] a = new int[n];
        System.out.print("請輸入N個數值:");
        for (int i = 0; i < n; i++) {
            a[i] = scanner.nextInt();
        }
        min = a[0];
        for (int i = 1; i < n; i++) {
            if (min > a[i]) {
                min = a[i];
            }
        }
        for (int i = 0; i < n; i++) {
            if (a[i] == min)
                s++;
        }
        System.out.println("最小值是" + min + "出現了" + s + "次");
    }
}
