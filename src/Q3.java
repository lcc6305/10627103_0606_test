import java.util.Scanner;

public class Q3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int num=1;
        int positive = 0;
        int negative = 0;
        int sum=0,str=-1;
        float eva;
        System.out.print("请输入一組整数（输入0结束程序）：");
        while(num!=0){
            num = scanner.nextInt();
            if(num>0)
                positive++;
            else if(num<0)
                negative++;
            sum+=num;
            str++;
        }
        eva=(float)sum/(float)str;
        System.out.println("總共正數:" + positive + " 總共負數:" + negative + " 總和:" + sum + " 平均:" + eva);
    }
}
