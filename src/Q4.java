import java.io.*;
import java.util.Random;

public class Q4 {
    public static void main(String[] args) throws IOException {
        Random ran = new Random();
        int ranone = ran.nextInt(+10) + 10;
        int[] rannumber;
        String a = "";
        rannumber = new int[ranone];
        System.out.println("共" + ranone + "筆");
        for (int i = 0; i < ranone; i++) {
            rannumber[i] = ran.nextInt(9999);
            a += rannumber[i] + " ";
        }
        FileWriter fw = new FileWriter("test.txt");
        fw.write(a);
        fw.flush();
        FileReader fr = new FileReader("test.txt");
        BufferedReader br = new BufferedReader(fr);
        while (br.ready()) {
            System.out.println(br.readLine());
        }
        fr.close();
    }
}

